import  tensorflow as tf
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Flatten, Dense, Dropout, BatchNormalization, Conv2D, MaxPool2D, Activation, MaxPooling2D
from tensorflow.keras.optimizers import Adam, SGD
from tensorflow.keras.preprocessing import image
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from tqdm import  tqdm
import keras

print(">> IMAGE CLASSIFICATION MODEL CREATION")

print()
print(">> STEP#1 IMAGE PRE-PROCESSING")
#train_image_generator = ImageDataGenerator(rescale=1.0/255) #zasto bas 1.0/255??
#test_image_generator = ImageDataGenerator(rescale=1.0/255)

#!git clone https://gitlab.com/thelastgrim/ori-projekat.git
#------------------

data = pd.read_csv('metadata/chest_xray_metadata.csv')
#data = pd.read_csv('/content/dataset-x-ray/Book1.csv')

print(data.columns)
data.shape
#---------------


img_width = 350
img_height = 350

X = []
#for i in tqdm(range(1500)):
for i in tqdm(range(data.shape[0])):
  path = 'metadata/'+ data['X_ray_image_name'][i]
  #path = '/content/dataset-x-ray/data/'+ data['X_ray_image_name'][i]
  img = image.load_img(path, target_size=(img_width,img_height,3))
  img = image.img_to_array(img)
  img = img/255.0
  X.append(img)


X = np.array(X)
X.shape

# import math
Y = []
# y = data.drop(['Unnamed: 0','X_ray_image_name','Label_2_Virus_category', 'Label_1_Virus_category'], axis=1)[0:1500]
# y = data.drop(['Unnamed: 0','X_ray_image_name', 'Label','Label_2_Virus_category'], axis=1)[0:1500]
y = data.drop(columns=['Unnamed: 0', 'X_ray_image_name', 'Label', 'Label_2_Virus_category'])[
    0:data.shape[0]]  # [0:1500]

print("PRE")
y.head()
print(y.dtypes)

# print(y)
y = y.to_numpy()
# y.shape
# for i in tqdm(range(1500)):
for i in tqdm(range(data.shape[0])):
  # print(y[i])
  if (pd.isnull(y[i])):
    label = [1, 0, 0]
  elif (y[i] == 'Virus'):
    label = [0, 1, 0]
  elif (y[i] == 'bacteria'):
    label = [0, 0, 1]
  Y.append(label)
  # print('jej')
  # print(y[i])
# y.dtype
print(Y)
Y = np.array(Y)
# print(Y)
print(y[1499])
print(y.shape)
Y.shape


X_train, X_test, y_train, y_test = train_test_split(X, Y, random_state =0, test_size=0.15)

#ALEXNET

#Instantiate an empty model
model = Sequential()

# 1st Convolutional Layer
model.add(Conv2D(filters=96, input_shape=(350,350,3), kernel_size=(11,11), strides=(4,4), padding='valid'))
model.add(Activation('relu'))
# Max Pooling
model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))

# 2nd Convolutional Layer
model.add(Conv2D(filters=256, kernel_size=(11,11), strides=(1,1), padding='valid'))
model.add(Activation('relu'))
# Max Pooling
model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))

# 3rd Convolutional Layer
model.add(Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding='valid'))
model.add(Activation('relu'))

# 4th Convolutional Layer
model.add(Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding='valid'))
model.add(Activation('relu'))

# 5th Convolutional Layer
model.add(Conv2D(filters=256, kernel_size=(3,3), strides=(1,1), padding='valid'))
model.add(Activation('relu'))
# Max Pooling
model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))

# Passing it to a Fully Connected layer
model.add(Flatten())
# 1st Fully Connected Layer
model.add(Dense(4096, input_shape=(350*350*3,)))
model.add(Activation('relu'))
# Add Dropout to prevent overfitting
model.add(Dropout(0.1))

# 2nd Fully Connected Layer
model.add(Dense(4096))
model.add(Activation('relu'))
# Add Dropout
model.add(Dropout(0.2))

# 3rd Fully Connected Layer
model.add(Dense(1000))
model.add(Activation('relu'))
# Add Dropout
model.add(Dropout(0.3))

# Output Layer
model.add(Dense(3))
model.add(Activation('softmax'))

model.summary()

  
# Compile the model
#opt = SGD(learning_rate=0.01, momentum=0.0, nesterov=False)
opt = SGD(learning_rate=0.001)

model.compile(loss = "categorical_crossentropy", optimizer = opt, metrics=["accuracy"])
#model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=["accuracy"])
epochs = 100


from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import Callback
from tensorflow.keras.callbacks import ModelCheckpoint
class EarlyStoppingByLossVal(Callback):
    def __init__(self, monitor='val_accuracy', value=0.00001, verbose=0):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current > 0.84:
          print(current)
          if self.verbose > 0:
              print("Epoch %05d: early stopping THR" % epoch)
          self.model.stop_training = True


callback = EarlyStoppingByLossVal(monitor='val_accuracy', value=0.00001, verbose=1)
mcp_save = ModelCheckpoint('.mdl_wts.hdf5', save_best_only=True, monitor='val_accuracy')

history = model.fit(X_train, y_train, epochs=epochs, validation_data=(X_test, y_test),  callbacks = [mcp_save])

number_of_epochs_it_ran = len(history.history['loss'])

model.load_weights('.mdl_wts.hdf5')


'''
score = model.evaluate(X_test, y_test, verbose = 1)
print("Test score: ", score[0])
print("Test accuracy: ", score[1])
'''
#ili ovo

test_loss, test_acc = model.evaluate(X_test,  y_test, verbose=2) # ovo je sa tensorflow sajta
print("Test loss: ", test_loss)
print("Test accuracy: ", test_acc)



#nodel.acc

print(">> STEP#4 VISUALIZING ACCURACY AND LOSS")
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']


loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(epochs)
#epochs_range = number_of_epochs_it_ran

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('ACCURACY')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper left')
plt.title('LOSS')

plt.show()

#preciznost
'''
from sklearn.metrics import confusion_matrix
pred = model.predict(X_test)
pred = np.argmax(pred,axis = 1) 
y_true = np.argmax(y_test,axis = 1)



CM = confusion_matrix(y_true, pred)
from mlxtend.plotting import plot_confusion_matrix
fig, ax = plot_confusion_matrix(conf_mat=CM ,  figsize=(5, 5))
plt.show()
'''
#TESTING
miss =0
hit = 0
#test_data = pd.read_csv('/content/ori-projekat/X-Ray-test/metadata/chest_xray_test_dataset.csv')

test_data = pd.read_csv('metadata-test/chest_xray_test_dataset.csv')
#--------------------------
Y_test_data = []
y_test_data = test_data.drop(columns=['Unnamed: 0', 'X_ray_image_name', 'Label', 'Label_2_Virus_category'])[0:test_data.shape[0]]
# print(y)
y_test_data = y_test_data.to_numpy()
# y.shape
for i in tqdm(range(624)):
#for i in tqdm(range(data.shape[0])):
  # print(y[i])
  if (pd.isnull(y_test_data[i])):
    label = [1, 0, 0]
  elif (y_test_data[i] == 'Virus'):
    label = [0, 1, 0]
  elif (y_test_data[i] == 'bacteria'):
    label = [0, 0, 1]
  Y_test_data.append(label)
  # print('jej')
  # print(y[i])
# y.dtype
#print(Y_test_data)
Y_test_data = np.array(Y_test_data)
# print(Y)
#print(y[1499])
print(y.shape)
Y_test_data.shape


#--------------------------
#test2 = data.drop(columns=['Unnamed: 0', 'Label', 'Label_2_Virus_category' ])[1340:1345]
#test2.head()
X_test_data = []
for i in tqdm(range(test_data.shape[0])):
#for i in tqdm(range(test_data.shape[0])):
  if (i == 624):
    break
  #k =  i+1340
  #path = '/content/ori-projekat/X-Ray-test/metadata/'+ str(test_data['X_ray_image_name'][i])
  path = 'metadata-test/'+ str(test_data['X_ray_image_name'][i])

  #print(test_data['X_ray_image_name'][i])
  klasa = test_data['Label_1_Virus_category'][i]
  #path = '/content/dataset-x-ray/data/'+ data['X_ray_image_name'][i]
  img = image.load_img(path, target_size=(img_width,img_height,3))
  img = image.img_to_array(img)
  img = img/255.0
  X_test_data.append(img)
  img = img.reshape(1,img_width,img_height,3) #3 je zbog tri kanala bojetj.rgb
  y_prob = model.predict(img) #vraca predikciju za svaku od target klasa
  #print(y_prob[0], " - ", klasa)
  if(y_prob[0][0] > y_prob[0][1] and y_prob[0][0]> y_prob[0][2]): #ako je predvideo da je normal
    if(pd.isnull(klasa)): #i ako stvarna klasa jeste normal
      hit = hit +1
    else:
        miss = miss +1
  elif(y_prob[0][1] > y_prob[0][0] and y_prob[0][1]> y_prob[0][2]): #ako je predvideo da je virus
    if(klasa == 'Virus'): #i ako stvarna klasa jeste virus
      hit = hit +1
    else:
        miss = miss +1
  elif(y_prob[0][2] > y_prob[0][0] and y_prob[0][2]> y_prob[0][1]): #ako je predvideo da je bakterija
    if(klasa == 'bacteria'): #i ako stvarna klasa jeste bakterija
      hit = hit +1
    else:
        miss = miss +1


X_test_data = np.array(X_test_data)
X_test_data.shape  
X_train, X_test, y_train, y_test = train_test_split(X_test_data, Y_test_data, random_state =0, test_size=0.15)


'''
score = model.evaluate(X_test, y_test, verbose = 1)
print("Test score: ", score[0])
print("Test accuracy: ", score[1])
'''
#ili ovo

test_loss, test_acc = model.evaluate(X_test,  y_test, verbose=2) # ovo je sa tensorflow sajta
print("Test loss: ", test_loss)
print("Test accuracy: ", test_acc)


print("HIT: " + str(hit) + "    MISS: " +  str(miss))
