# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""
from random import randrange

import util
from game import Directions


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """
    "*** YOUR CODE HERE ***"

    # Create stack of nodes and initialize it with start state and stop direction
    # Cost of the node is not necessary to store since it is not relevant to dfs
    nodesStack = util.Stack()
    startNode = (problem.getStartState(), [Directions.STOP])
    nodesStack.push(startNode)

    # Create empty set of visited nodes
    visitedNodes = set()

    # Perform dfs
    while True:
        node = nodesStack.pop()
        nodeState = node[0]
        nodeDirections = node[1]

        # If goal state is reached, return list of directions from startState to goalState
        if problem.isGoalState(nodeState):
            nodeDirections.pop(0)
            return nodeDirections

        # Visit node if not already
        elif nodeState not in visitedNodes:
            # Remember visited node
            visitedNodes.add(nodeState)

            # Store child nodes in stack to expand in next iteration
            for child in problem.getSuccessors(nodeState):
                childState = child[0]
                childDirections = child[1]

                # Must not append to nodeDirections because new list of directions is needed for each child node
                newNodeChild = (childState, nodeDirections + [childDirections])
                nodesStack.push(newNodeChild)

        else:
            """print "Node " + str(nodeState) + " already visited"""

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"

    # Create queue of nodes and initialize it with start state and stop direction
    # Cost of the node is not necessary to store since it is not relevant to bfs
    nodesQueue = util.Queue()
    startNode = (problem.getStartState(), [Directions.STOP])
    nodesQueue.push(startNode)

    # Create empty set of visited nodes
    visitedNodes = set()

    # Perform bfs
    while True:
        node = nodesQueue.pop()
        nodeState = node[0]
        nodeDirections = node[1]

        # If goal state is reached, return list of directions from startState to goalState
        if problem.isGoalState(nodeState):
            nodeDirections.pop(0)
            return nodeDirections

        # Visit node if not already
        elif nodeState not in visitedNodes:
            # Remember visited node
            visitedNodes.add(nodeState)

            # Store child nodes in queue to expand in next iteration
            for child in problem.getSuccessors(nodeState):
                childState = child[0]
                childDirections = child[1]

                # Must not append to nodeDirections because new list of directions is needed for each child node
                newNodeChild = (childState, nodeDirections + [childDirections])
                nodesQueue.push(newNodeChild)

        else:
            """print "Node " + str(nodeState) + " already visited"""

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"

    # Create lambda function which calculates cost of actions in order to get to referenced node
    # This function will be used in priority queue
    costOfActions = lambda path: problem.getCostOfActions(path)

    # Create priority queue of nodes and initialize it with start state and stop direction
    # Since ucs need cost of actions, each node will have that cost as third element calculated with lambda function
    # wich will be passed to priority queue
    nodesQueue = util.PriorityQueue()

    # Create start node
    startNodeState = problem.getStartState()
    startNodeDirection = [Directions.STOP]
    startNodeCost = costOfActions(startNodeDirection)
    startNode = (startNodeState, startNodeDirection, startNodeCost)

    # Initialize priority queue with start state and stop direction
    nodesQueue.push(startNode, startNodeCost)

    # Create empty set of visited nodes
    visitedNodes = set()

    # Perform ucs
    while True:
        node = nodesQueue.pop()
        nodeState = node[0]
        nodeDirections = node[1]

        # If goal state is reached, return list of directions from startState to goalState
        if problem.isGoalState(nodeState):
            nodeDirections.pop(0)           # remove Directions.STOP
            return nodeDirections

        # Visit node if not already
        elif nodeState not in visitedNodes:
            # Remember visited node
            visitedNodes.add(nodeState)

            # Store child nodes in queue to expand in next iteration
            for child in problem.getSuccessors(nodeState):
                childState = child[0]

                # Must not append to nodeDirections because new list of directions is needed for each child node
                childDirections = nodeDirections + [child[1]]
                childNodeCost = costOfActions(childDirections)

                newNodeChild = (childState, childDirections, childNodeCost)
                nodesQueue.push(newNodeChild, childNodeCost)

        else:
            """print "Node " + str(nodeState) + " already visited"""


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    # Create lambda function which calculates cost of actions in order to get to referenced node
    # This function will be used in priority queue
    evaluationFunction = lambda state, path: problem.getCostOfActions(path) + heuristic(state, problem)

    # Create priority queue of nodes and initialize it with start state and stop direction
    # Since ucs need cost of actions, each node will have that cost as third element calculated with lambda function
    # wich will be passed to priority queue
    nodesQueue = util.PriorityQueue()

    # Create start node
    startNodeState = problem.getStartState()
    startNodeDirection = [Directions.STOP]
    startNodeCost = evaluationFunction(startNodeState, startNodeDirection)
    startNode = (startNodeState, startNodeDirection, startNodeCost)

    # Initialize priority queue with start state and stop direction
    nodesQueue.push(startNode, startNodeCost)

    # Create empty set of visited nodes
    visitedNodes = set()

    # Perform ucs
    while True:
        node = nodesQueue.pop()
        nodeState = node[0]
        nodeDirections = node[1]

        # If goal state is reached, return list of directions from startState to goalState
        if problem.isGoalState(nodeState):
            nodeDirections.pop(0)  # remove Directions.STOP
            return nodeDirections

        # Visit node if not already
        elif nodeState not in visitedNodes:
            # Remember visited node
            visitedNodes.add(nodeState)

            # Store child nodes in queue to expand in next iteration
            for child in problem.getSuccessors(nodeState):
                childState = child[0]

                # Must not append to nodeDirections because new list of directions is needed for each child node
                childDirections = nodeDirections + [child[1]]
                childNodeCost = evaluationFunction(childState, childDirections)

                newNodeChild = (childState, childDirections, childNodeCost)
                nodesQueue.push(newNodeChild, childNodeCost)

        else:
            """print "Node " + str(nodeState) + " already visited"""


def foodHeuristic(state, problem):
    """
    Calculating heuristic to closest food
    :param state: pacmans position
    :param problem: given search problem
    :return: estimated distance to solution (closest food)
    """
    # Get food as list
    foodList = problem.food.asList()

    # If there is no food on map, return 0
    if len(foodList) == 0:
        return 0

    # Get coordinates of given pacman state
    x1, y1 = state

    # Save distance for each food
    distances = []
    for x2, y2 in foodList:
        distances.append(util.manhattanDistance((x1, y1), (x2, y2)))

    # Get distance to closest food
    closest = min(distances)
    distances.remove(closest)

    # Calculate average pacmans distance from each food
    # with this pacman should to go where there are more food
    try:
        avg = sum(distances)/len(distances)
    except:
        avg = 0

    return closest + avg

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
