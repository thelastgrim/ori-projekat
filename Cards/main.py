import pandas as pd
import seaborn as sb
from sklearn.cluster import KMeans
import sklearn
from matplotlib import pyplot as plt


def main():

    data = pd.read_csv('credit_card_data.csv', sep=',')

    print(str(data.shape[0]) + " redova i "+str(data.shape[1]) + " kolona.")
    print("Null vrednosti: " + str(data.isnull().sum().values.sum()))

    #  popunjavanje null vrednosti meanom
    data = data.fillna(data.mean())

    print("Null vrednosti nakon mean: " + str(data.isnull().sum().values.sum()))

    # izbacivaje cust _id
    data = data.drop("CUST_ID", axis=1)

    print(str(data.shape[0]) + " redova i " + str(data.shape[1]) + " kolona. (uklonjen CUST_ID)")

    # Eksplorativna analiza grafovi

    data.hist(figsize=(15, 15), grid=True)
    plt.savefig('plots.png')
    plt.clf()
    data[['BALANCE_FREQUENCY', 'PURCHASES_FREQUENCY', 'ONEOFF_PURCHASES_FREQUENCY', 'PURCHASES_INSTALLMENTS_FREQUENCY',
          'CASH_ADVANCE_FREQUENCY', 'PRC_FULL_PAYMENT']]\
        .plot.box(figsize=(15, 10))
    plt.savefig('box_plots_probability.png')
    plt.clf()
    data[['CASH_ADVANCE_TRX', 'PURCHASES_TRX']]\
        .plot.box(figsize=(15, 10))
    plt.savefig('box_plots_transactions.png')
    plt.clf()
    data[['BALANCE', 'PURCHASES', 'ONEOFF_PURCHASES', 'INSTALLMENTS_PURCHASES', 'CASH_ADVANCE',
          'CREDIT_LIMIT', 'PAYMENTS', 'MINIMUM_PAYMENTS']] \
        .plot.box(figsize=(15, 10))
    plt.savefig('box_plots_amount.png')

    # izbacivanje pogresnih vrednosti (neke verovatnoce su > od 1)
    data = data[(data[['CASH_ADVANCE_FREQUENCY']] <= 1).all(axis=1)]
    print(str(data.shape[0]) + " redova i " + str(data.shape[1]) + " kolona. (uklonjene vrednosti sa P>1)")

    plt.figure(figsize=(15, 10))
    sb.heatmap(data.corr(), cmap='YlGnBu', annot=True)
    plt.savefig('corelation.png')

    # Skaliranje
    print ('Skaliranje podataka zbog preciznosti '
           '(postoije 3 tipa razlicitih vrednosti: verovatnoca, iznos, broj transkacija)')
    mms = sklearn.preprocessing.MinMaxScaler(feature_range=(0, 1))
    new_data = pd.DataFrame(mms.fit_transform(data.values), columns=data.columns)
    new_data_scaled = pd.DataFrame(mms.fit_transform(data.values), columns=data.columns)

    # Elbow method za odredjivanje optimalnog broja klastera

    nodes = []

    for i in range(1, 17):
        kmeans = KMeans(n_clusters=i, init='random', random_state=101)
        kmeans.fit(new_data)
        nodes.append(kmeans.inertia_)

    plt.figure(figsize=(15, 5))
    plt.plot(range(1, 17), nodes)
    # 6 CLUSTERA iz elbow metode

    kmeans = KMeans(n_clusters=6, random_state=101)

    # varijanca, koliko elemenata procentualno opisuje skup
    variance = []
    for n in range(1, 18):
        pca = sklearn.decomposition.PCA(n_components=n)
        new_data_pca = pca.fit(new_data)
        variance.append(sum(new_data_pca.explained_variance_ratio_))

    print ("Varijansa:\n(n --- %)")
    c = 1

    for v in variance:
        print (str(c) + " --- " + str(v))
        c = c+1

    # biramo (6) jer opisuju >90%

    plt.clf()
    plt.plot(variance, [i for i in range(1, len(variance)+1)], alpha=0.5)
    plt.grid()
    plt.xlabel('Varijansa', fontsize=18)
    plt.ylabel('Broj promenljivih', fontsize=16)
    plt.savefig("variance.png")

    # Redukcija na samo 6 komponenti
    new_data_pca = sklearn.decomposition.PCA(n_components=6).fit(new_data)

    new_data_trans = new_data_pca.fit_transform(new_data)
    new_data = pd.DataFrame(new_data_trans)

    # klasterovanje
    kmeans = kmeans.fit(new_data)

    print ("group   ITEMS")
    print (pd.Series(kmeans.labels_).value_counts())

    colors_dict = {0: 'red', 1: 'green', 2: 'blue', 3: 'cyan', 4: 'yellow', 5: 'purple'}
    colors_list = []

    for i in kmeans.labels_:
        colors_list.append(colors_dict[i])

    plt.figure(figsize=(5, 5))
    plt.scatter(new_data_trans[:, 0], new_data_trans[:, 1], color=colors_list, alpha=0.5)
    plt.savefig("clusters_2d.png")

    # sumarizacija bitnih atributa
    sumarization = ['CREDIT_LIMIT', 'BALANCE', 'PURCHASES', 'ONEOFF_PURCHASES', 'INSTALLMENTS_PURCHASES',
                    'CASH_ADVANCE', 'PAYMENTS', 'MINIMUM_PAYMENTS']
    data = pd.DataFrame(new_data_scaled[sumarization])

    label = kmeans.fit_predict(data)
    data['group'] = label
    sumarization.append('group')

    sb.pairplot(data[sumarization], x_vars=sumarization[:-1], y_vars=['group'])
    plt.savefig('sumarization.png')

    print ('''
grupa5
	-srednji limit na kreditnim karticama
	-kupuju skuplje proizvode, uglavnom jednokratno
	-vise uplacuju od prosecnog korisnika, sa malim iznosom uplacenim unapred

grupa4
    -konzistentant srednji limit na kreditnim karticama
	-kupuju jeftinije proizvode, cesce na rate
	-vise uplacuju od prosecno korisnika, a manje od C5, sa velikim iznosima uplacenim unapred

grupa3 (studenti)
	-konzistentant najmanji limit na kreditnim karticma od svih korisnika
	-malo novca na racunu za kupovinu jetinijih proizvoda
	-podjednako jednokratno i na rate

grupa2
	-srednji limit na kreditnim karticama
	-kupuju jeftinije proizode, cesce na rate
	-ukupne uplate variraju izmedju najmanjih i srednjih iznos

grupa1
	-manji limit na kreditnim karticama
	-kupuju jeftinije proizode, uglavnom jednokratno
	-minimalne uplate variraju od jako malih do srednje velikih

grupa0
	-najmanje novca na racunu
	-podjednaka kupovina jednokratno i na rate
	-najmanje novca uplaceno unapred sa najmanjim minimalnim uplatama
''')


if __name__ == '__main__':
    main()